/**
 * Configuration for copy task(s)
 */
'use strict';

var taskConfig = function(grunt) {

    grunt.config.set('copy', {
        server: {
            files: [{
                expand: true,
                cwd: '<%= yeogurt.client %>/',
                dest: '<%= yeogurt.staticServer %>/',
                src: [
                    'scripts/**/*.{js,json}',
                    'dashboard/**/*.*',
                    'bower_components/**/*.{js,map,css,woff,otf,ttf,eot,svg}',
                    'styles/**/*.css',
                    'images/**',
                    'videos/**',
                    'files/**',
                    '*.{ico,png,txt}',
                    'styles/fonts/**/*.{woff,otf,ttf,eot,svg}',
                ]
            }]
        },
        dist: {
            files: [{
                expand: true,
                cwd: '<%= yeogurt.client %>/',
                dest: '<%= yeogurt.dist %>/',
                src: [
                    'scripts/**/*.{js,json}',
                    'bower_components/modernizr/modernizr.js',
                    'bower_components/**/*.{js,map,css,woff,otf,ttf,eot,svg}',
                    'dashboard/**/*.*',
                    '!*.js',
                    '*.{ico,png,txt}',
                    'videos/**',
                    'images/**/*.{webp}',
                    'styles/fonts/**/*.{woff,otf,ttf,eot,svg}'
                ]
            }]
        }
    });

};

module.exports = taskConfig;

/**
 *   Application Logic
 */
'use strict';



// usage: log('inside coolFunc',this,arguments);
// http://paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
window.log = function() {
    log.history = log.history || []; // store logs to an array for reference
    log.history.push(arguments);
    if (this.console) {
        z
        console.log(Array.prototype.slice.call(arguments));
    }
};

$(document).ready(function() {

    $('[data-toggle="tooltip"]').tooltip()
        //hide drop downs
        // $("#DropdownBlock_1").hide("fast");
    $("#DropdownBlock_2").hide("fast");
    $("#DropdownBlock_3").hide("fast");
    $("#DropdownBlock_4").hide("fast");
    $("#DropdownBlock_5").hide("fast");

    $("#DropdownBlock_6, #DropdownBlock_7, #DropdownBlock_8, #DropdownBlock_9, #DropdownBlock_10").hide("fast");
    $("#DropdownBlock_11, #DropdownBlock_12, #DropdownBlock_13, #DropdownBlock_14, #DropdownBlock_16, #DropdownBlock_15").hide("fast");
    //hide modal drop down

    $("#ModalDropdownBlock_2").hide("fast");
    $("#ModalDropdownBlock_3").hide("fast");
    $("#ModalDropdownBlock_4").hide("fast");
    $("#ModalDropdownBlock_5").hide("fast");

    $("#ModalDropdownBlock_6, #ModalDropdownBlock_7, #ModalDropdownBlock_8, #ModalDropdownBlock_9, #ModalDropdownBlock_10").hide("fast");
    $("#ModalDropdownBlock_11, #ModalDropdownBlock_12, #ModalDropdownBlock_13, #ModalDropdownBlock_14, #ModalDropdownBlock_16, #ModalDropdownBlock_15").hide("fast");

    // hide all sub page modal 
    $("#ModalDropdownBlock_2").hide("fast");


    //tooltip 
    $(".whygroup").popover('show');

    // responsive video

    var video = $(".video-player");
    var windowObj = $(window);

    function onResizeWindow() {
        resizeVideo(video[0]);
    }

    function onLoadMetaData(e) {
        resizeVideo(e.target);
    }

    function resizeVideo(videoObject) {
        var percentWidth = videoObject.clientWidth * 100 / videoObject.videoWidth;
        var videoHeight = videoObject.videoHeight * percentWidth / 100;
        video.height(videoHeight);
    }

    video.on("loadedmetadata", onLoadMetaData);
    windowObj.resize(onResizeWindow);


    // ============================================== get json function ============================================= 


    var MobilePlansList = function() {
        $.getJSON("scripts/AZ-Plans.json", function(data) {

            var templateSource = $("#PlansListMobileTemplate").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#PlansListMobile').html(article_html);

        });
    }
    MobilePlansList();

    var getData = function() {
        $.getJSON("scripts/AZ-Plans.json", function(data) {

            var templateSource = $("#cont").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#Plans').html(article_html);
            $(".planOption").hide("easeOut", function() {
                hideAndShowPlans();
            });

        });
    }
    getData();
    var ScriptTableData = function() {
        $.getJSON("scripts/la-plans.json", function(data) {

            var templateSource = $("#PrescriptionData").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#PrescriptionPlans').html(article_html);
        });
    }
    ScriptTableData();



    // ===================== drop downs =========================================================================== //




    // keeping state vars 

    var planPicked;
    var oopmPicked;
    var nativeAmericanStatus = false;
    $("#nonNativeAmericanCheck").prop("checked", true);
    // $("#modalnonNativeAmericanCheck").prop("checked", true);


    console.log("native american status set to: " + nativeAmericanStatus);

    var setPlanPicked = function(pp) {
        planPicked = pp;
        console.log("plan set at " + planPicked);
    }

    var setoopmPicked = function(oopmPicked) {
        oopmPicked = oopmPicked;
        console.log("OOPM set at " + oopmPicked);
    };


    var callSatellite = function(status) {

        var nativeBooleen = status;
        console.log("satellite function called status set to  " + nativeBooleen);
        _satellite.setVar('NA Status ', nativeBooleen);
        _satellite.notify('NA Status: ' + nativeBooleen);
        _satellite.track('NA Status=' + nativeBooleen);

    }

    var setnativeAmericanStatus = function(naStatus) {
        nativeAmericanStatus = naStatus;
        console.log("NA status set at " + nativeAmericanStatus);
        // _satellite.setVar('NA Status ', nativeAmericanStatus);
        // _satellite.notify('NA Status: ' + nativeAmericanStatus);
        // _satellite.track('NA Status='+ nativeAmericanStatus);

        callSatellite(naStatus);
    };


    var removeCata = function() {
        $("#ModalDropdown6 > div > ul > li:nth-child(43) > a").css("display", "none");
        $("#Dropdown6 > div > ul > li:nth-child(43) > a").css("display", "none");

    }

    var showCata = function() {
        $("#Dropdown > div > ul > li:nth-child(43) > a").css("display", "block");
    }

    // $("#Dropdown6 > div > ul > li:nth-child(43) > a").css("display", "none");

    // =========================================== native american on click event handlers ===================================== // 

    // nonNativeAmericanCheck.bind("click", callSatel)


    $("#nonNativeAmericanCheck").on("click", function() {
        $("#DropdownBlock_6").slideUp("easeOut");
        $("#DropdownBlock_2").hide();
        $("#nativeAmericanCheck").prop("checked", false);
        $("#DropdownBlock_1").slideDown("easeIn", function() {
            hideMasterPlanFalse();
            // hideAllNative()
            setnativeAmericanStatus(false);
            showCata();
            console.log("new native american status set to: " + nativeAmericanStatus);
        });


    });

    $("#nativeAmericanCheck").on("click", function() {
        $("#DropdownBlock_1").slideUp("easeOut");
        $("#DropdownBlock_6").slideDown("easeIn");
        $("#DropdownBlock_2").hide();
        hideMasterPlanFalse();
        $("#nonNativeAmericanCheck").prop("checked", false);
        // hideAllnonNative();
        setnativeAmericanStatus(true);
        removeCata();

    });

    // modal radio for find a plan

    $("#modalnonNativeAmericanCheck").on("click", function() {
        $("#ModalDropdownBlock_6").slideUp("easeOut");
        $("#ModalDropdownBlock_2").hide();
        $("#modalnativeAmericanCheck").prop("checked", false);
        $("#ModalDropdownBlock_1").slideDown("easeIn", function() {
            ModalhideMasterPlanFalse();
            // hideAllNative()
            setnativeAmericanStatus(false);
            showCata();
            console.log("new native american status set to: " + nativeAmericanStatus);
            callSatellite(false);
        });


    });

    $("#modalnativeAmericanCheck").on("click", function() {
        $("#ModalDropdownBlock_1").slideUp("easeOut");
        $("#ModalDropdownBlock_6").slideDown("easeIn");
        $("#ModalDropdownBlock_2").hide();
        ModalhideMasterPlanFalse();
        $("#modalnonNativeAmericanCheck").prop("checked", false);
        // hideAllnonNative();
        setnativeAmericanStatus(true);
        callSatellite(true)
        removeCata();

    });



    var hideMasterPlanFalse = function() { // show only master plans
            $("#Dropdown > div > ul > li> a[data-MasterTorF=FALSE]").fadeOut("fast");
            $("#Dropdown6 > div > ul > li> a[data-MasterTorF=FALSE]").fadeOut("fast");


            $("#Dropdown > div > ul > li> a[data-MasterTorF=TRUE]").bind("click", function() {
                event.preventDefault();

                var dataNameVal = $(this).data("plannameshort");
                var droptext = $(".dynaLabel");
                droptext.text(dataNameVal);


                console.log("the plan you picked is " + dataNameVal); // plan picked

                if (dataNameVal == "Platinum 250") {
                    console.log("going platnium");
                    window.location.href = "./platinum-250.html";
                }

                if (dataNameVal == "Gold 1000") {
                    console.log("going gold");
                    window.location.href = "./gold-1000.html";
                }

                if (dataNameVal == "Gold 1500") {
                    console.log("going gold");
                    window.location.href = "./gold-1500.html";
                }

                if (dataNameVal == "Gold HSA 1300") {
                    console.log("going gold");
                    window.location.href = "./gold-HSA-1300.html";
                }

                if (dataNameVal == "Bronze 4200") {
                    console.log("going bronze");
                    window.location.href = "./bronze-4200.html";
                }

                if (dataNameVal == "Bronze HSA 6275") {
                    console.log("going bronze");
                    window.location.href = "./bronze-HSA-6275.html";
                }

                if (dataNameVal == "Catastrophic 6600") {
                    console.log("going bronze");
                    window.location.href = "./catastrophic-6600.html";
                }

                if (dataNameVal == "Silver 2000") {
                    $("#DropdownBlock_4, #DropdownBlock_3, #DropdownBlock_2").slideUp();
                    $("#DropdownBlock_5").slideDown();
                }

                if (dataNameVal == "Silver 4000") {
                    $("#DropdownBlock_5, #DropdownBlock_3, #DropdownBlock_2").slideUp();
                    $("#DropdownBlock_4").slideDown();
                }

                if (dataNameVal == "Silver HSA 3600") {
                    $("#DropdownBlock_2, #DropdownBlock_4, #DropdownBlock_5").slideUp();
                    $("#DropdownBlock_3").slideDown();
                }

                if (dataNameVal == "Silver  5000") {
                    $("#DropdownBlock_3, #DropdownBlock_4, #DropdownBlock_5").slideUp();
                    $("#DropdownBlock_2").slideDown();
                }


            });


            $("#Dropdown6 > div > ul > li> a[data-MasterTorF=TRUE]").bind("click", function() {
                event.preventDefault();
                var dataNameVal = $(this).data("plannameshort");
                var droptext = $(".dynaLabel");
                droptext.text(dataNameVal);
                console.log("test");

                if (dataNameVal == "Platinum 250") {
                    console.log("going platnium");
                    // window.location.href = "./platinum-250.html";
                    $("#DropdownBlock_10, #DropdownBlock_9, #DropdownBlock_8, #DropdownBlock_12, #DropdownBlock_13, #DropdownBlock_14, #DropdownBlock_15, #DropdownBlock_16").slideUp();
                    $("#DropdownBlock_11").slideDown();
                }

                if (dataNameVal == "Gold 1000") {
                    console.log("going gold");
                    // window.location.href = "./gold-1000.html";
                    $("#DropdownBlock_10, #DropdownBlock_9, #DropdownBlock_8, #DropdownBlock_11, #DropdownBlock_13, #DropdownBlock_14, #DropdownBlock_15, #DropdownBlock_16").slideUp();
                    $("#DropdownBlock_12").slideDown();
                }

                if (dataNameVal == "Gold 1500") {
                    console.log("going gold");
                    // window.location.href = "./gold-1500.html";
                    $("#DropdownBlock_10, #DropdownBlock_9, #DropdownBlock_8, #DropdownBlock_11, #DropdownBlock_12, #DropdownBlock_14, #DropdownBlock_15, #DropdownBlock_16").slideUp();
                    $("#DropdownBlock_13").slideDown();

                }

                if (dataNameVal == "Gold HSA 1300") {
                    console.log("going gold");
                    // window.location.href = "./gold-HSA-1300.html";
                    $("#DropdownBlock_10, #DropdownBlock_9, #DropdownBlock_8, #DropdownBlock_11, #DropdownBlock_12, #DropdownBlock_13, #DropdownBlock_15, #DropdownBlock_16").slideUp()
                    $("#DropdownBlock_14").slideDown();
                }

                if (dataNameVal == "Bronze 4200") {
                    console.log("going bronze");
                    // window.location.href = "./bronze-4200.html";
                    $("#DropdownBlock_10, #DropdownBlock_9, #DropdownBlock_8, #DropdownBlock_11, #DropdownBlock_12, #DropdownBlock_13, #DropdownBlock_14, #DropdownBlock_16").slideUp();
                    $("#DropdownBlock_15").slideDown();
                }

                  if (dataNameVal == "Bronze HSA 6275") {
                      console.log("going bronze");
                      // window.location.href = "./bronze-HSA-6275.html";
                $("#DropdownBlock_10, #DropdownBlock_9, #DropdownBlock_8, #DropdownBlock_11, #DropdownBlock_12, #DropdownBlock_13, #DropdownBlock_14, #DropdownBlock_15")
                      $("#DropdownBlock_16").slideDown();
                  }

                // if (dataNameVal == "Catastrophic  6600") {
                //     console.log("going bronze");
                //     window.location.href = "./catastrophic-6600.html";
                // }

                if (dataNameVal == "Silver 2000") {
                    $("#DropdownBlock_7, #DropdownBlock_8, #DropdownBlock_9").slideUp();
                    $("#DropdownBlock_10").slideDown();
                }

                if (dataNameVal == "Silver 4000") {
                    $("#DropdownBlock_7, #DropdownBlock_8, #DropdownBlock_10").slideUp();
                    $("#DropdownBlock_9").slideDown();
                }

                if (dataNameVal == "Silver HSA 3600") {
                    $("#DropdownBlock_9, #DropdownBlock_7, #DropdownBlock_10").slideUp();
                    $("#DropdownBlock_8").slideDown();
                }

                if (dataNameVal == "Silver  5000") {
                    $("#DropdownBlock_8, #DropdownBlock_9, #DropdownBlock_10").slideUp();
                    $("#DropdownBlock_7").slideDown();
                }


            });

        }
        // ====================================== end of logic for home page find a plan tool ============= //

    // ================================ MODAL LOGIC ======================================================

    var ModalhideMasterPlanFalse = function() { // show only master plans
        $("#ModalDropdown > div > ul > li> a[data-MasterTorF=FALSE]").hide("fast");
        $("#ModalDropdown6 > div > ul > li> a[data-MasterTorF=FALSE]").hide("fast");



        $("#ModalDropdown > div > ul > li> a[data-MasterTorF=TRUE]").bind("click", function() {
            event.preventDefault();

            var dataNameVal = $(this).data("plannameshort");
            var droptext = $(".dynaLabel");
            droptext.text(dataNameVal);

            console.log("the plan you picked is " + dataNameVal); // plan picked


            if (dataNameVal == "Platinum 250") {
                console.log("going platnium");
                window.location.href = "./platinum-250.html";
            }

            if (dataNameVal == "Gold 1000") {
                console.log("going gold");
                window.location.href = "./gold-1000.html";
            }

            if (dataNameVal == "Gold 1500") {
                console.log("going gold");
                window.location.href = "./gold-1500.html";
            }

            if (dataNameVal == "Gold HSA 1300") {
                console.log("going gold");
                window.location.href = "./gold-HSA-1300.html";
            }

            if (dataNameVal == "Bronze 4200") {
                console.log("going bronze");
                window.location.href = "./bronze-4200.html";
            }

            if (dataNameVal == "Bronze HSA 6275") {
                console.log("going bronze");
                window.location.href = "./bronze-HSA-6275.html";
            }

            if (dataNameVal == "Catastrophic 6600") {
                console.log("going bronze");
                window.location.href = "./catastrophic-6600.html";
            }

            if (dataNameVal == "Silver 2000") {
                $("#ModalDropdownBlock_4,#ModalDropdownBlock_3,#ModalDropdownBlock_2").slideUp();
                $("#ModalDropdownBlock_5").slideDown();
            }

            if (dataNameVal == "Silver 4000") {
                $("#ModalDropdownBlock_5,#ModalDropdownBlock_3,#ModalDropdownBlock_2").slideUp();
                $("#ModalDropdownBlock_4").slideDown();
            }

            if (dataNameVal == "Silver HSA 3600") {
                $("#ModalDropdownBlock_2,#ModalDropdownBlock_4,#ModalDropdownBlock_5").slideUp();
                $("#ModalDropdownBlock_3").slideDown();
            }

            if (dataNameVal == "Silver  5000") {
                $("#ModalDropdownBlock_3,#ModalDropdownBlock_4,#ModalDropdownBlock_5").slideUp();
                $("#ModalDropdownBlock_2").slideDown();
            }


        });


        $("#ModalDropdown6 > div > ul > li> a[data-MasterTorF=TRUE]").bind("click", function() {
            event.preventDefault();
            var dataNameVal = $(this).data("plannameshort");
            var droptext = $(".dynaLabel");
            droptext.text(dataNameVal);
            console.log("you picked plan" + droptext);

            if (dataNameVal == "Platinum 250") {
                console.log("going platnium");
                // window.location.href = "./platinum-250.html";
                $("#ModalDropdownBlock_10,#ModalDropdownBlock_9,#ModalDropdownBlock_8,#ModalDropdownBlock_12,#ModalDropdownBlock_13, #ModalDropdownBlock_14,#ModalDropdownBlock_15,#ModalDropdownBlock_16").slideUp();
                $("#ModalDropdownBlock_11").slideDown();
            }

            if (dataNameVal == "Gold 1000") {
                console.log("going gold");
                // window.location.href = "./gold-1000.html";
                $("#ModalDropdownBlock_10,#ModalDropdownBlock_9,#ModalDropdownBlock_8,#ModalDropdownBlock_11,#ModalDropdownBlock_13, #ModalDropdownBlock_14,#ModalDropdownBlock_15,#ModalDropdownBlock_16").slideUp();;
                $("#ModalDropdownBlock_12").slideDown();
            }

            if (dataNameVal == "Gold 1500") {
                console.log("going gold");
                // window.location.href = "./gold-1500.html";
                $("#ModalDropdownBlock_10,#ModalDropdownBlock_9,#ModalDropdownBlock_8,#ModalDropdownBlock_11,#ModalDropdownBlock_12, #ModalDropdownBlock_14,#ModalDropdownBlock_15,#ModalDropdownBlock_16").slideUp();;
                $("#ModalDropdownBlock_13").slideDown();

            }

            if (dataNameVal == "Gold HSA 1300") {
                console.log("going gold");
                // window.location.href = "./gold-HSA-1300.html";
                $("#ModalDropdownBlock_10,#ModalDropdownBlock_9,#ModalDropdownBlock_8,#ModalDropdownBlock_11,#ModalDropdownBlock_12, #ModalDropdownBlock_13,#ModalDropdownBlock_15,#ModalDropdownBlock_16").slideUp();
                $("#ModalDropdownBlock_14").slideDown();
            }

            if (dataNameVal == "Bronze 4200") {
                console.log("going bronze");
                // window.location.href = "./bronze-4200.html";
                $("#ModalDropdownBlock_10,#ModalDropdownBlock_9,#ModalDropdownBlock_8,#ModalDropdownBlock_11,#ModalDropdownBlock_12, #ModalDropdownBlock_13,#ModalDropdownBlock_14,#ModalDropdownBlock_16").slideUp();
                $("#ModalDropdownBlock_15").slideDown();
            }

            if (dataNameVal == "Bronze  HSA 6275") {
                console.log("going bronze");
                // window.location.href = "./bronze-HSA-6275.html";
                $("#ModalDropdownBlock_10,#ModalDropdownBlock_9,#ModalDropdownBlock_8,#ModalDropdownBlock_11,#ModalDropdownBlock_12, #ModalDropdownBlock_13,#ModalDropdownBlock_14,#ModalDropdownBlock_15").slideUp();
                $("#ModalDropdownBlock_16").slideDown();
            }

            // if (dataNameVal == "Catastrophic  6600") {
            //     console.log("going bronze");
            //     window.location.href = "./catastrophic-6600.html";
            // }

            if (dataNameVal == "Silver 2000") {
                $("#ModalDropdownBlock_7,#ModalDropdownBlock_8,#ModalDropdownBlock_9").slideUp();
                $("#ModalDropdownBlock_10").slideDown();
            }

            if (dataNameVal == "Silver 4000") {
                $("#ModalDropdownBlock_7,#ModalDropdownBlock_8,#ModalDropdownBlock_10").slideUp();
                $("#ModalDropdownBlock_9").slideDown();
            }

            if (dataNameVal == "Silver HSA 3600") {
                $("#ModalDropdownBlock_9,#ModalDropdownBlock_7,#ModalDropdownBlock_10").slideUp();
                $("#ModalDropdownBlock_8").slideDown();
            }

            if (dataNameVal == "Silver  5000") {
                $("#ModalDropdownBlock_8,#ModalDropdownBlock_9,#ModalDropdownBlock_10").slideUp();
                $("#ModalDropdownBlock_7").slideDown();
            }



        });

    }

    // ======================================== sub page modal dropdown ================================ //




    // ==================================================================================================    
    // native american plans 

    // hide not applicables list items

    var hideNA = function() {
        $("#Dropdown2 > div > ul > li> a[data-OOPInFam=na]").hide();
        $("#Dropdown2 > div > ul > li> a[data-OOPInInd=na]").hide();
        $("#ModalDropdown2 > div > ul > li> a[data-OOPInFam=na]").hide();
        $("#ModalDropdown2 > div > ul > li> a[data-OOPInInd=na]").hide();
    }

    var dropdownList = function() {
        $.getJSON("scripts/la-plans.json", function(data) {

            var templateSource = $("#DropdownTemplate").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#Dropdown').html(article_html);
            hideMasterPlanFalse();


        });
    }
    dropdownList(); // master plans


    var dropdownList2 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#DropdownTemplate2").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#Dropdown2').html(article_html);
            hideNA();

        });
    }
    dropdownList2(); // oopm


    var dropdownList3 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#DropdownTemplate3").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#Dropdown3').html(article_html);

        });
    }
    dropdownList3(); // silver HSA 3600 plan

    var dropdownList4 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#DropdownTemplate4").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#Dropdown4').html(article_html);

        });
    }
    dropdownList4();



    var dropdownList5 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#DropdownTemplate5").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#Dropdown5').html(article_html);

        });
    }
    dropdownList5();

    var dropdownList6 = function() {
        $.getJSON("scripts/la-plans.json", function(data) {

            var templateSource = $("#DropdownTemplate6").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#Dropdown6').html(article_html);

        });
    }
    dropdownList6();

    var dropdownList7 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#DropdownTemplate7").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#Dropdown7').html(article_html);

        });
    }
    dropdownList7();

    var dropdownList8 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#DropdownTemplate8").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#Dropdown8').html(article_html);

        });
    }
    dropdownList8();


    var dropdownList9 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#DropdownTemplate9").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#Dropdown9').html(article_html);

        });
    }
    dropdownList9();


    var dropdownList10 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#DropdownTemplate10").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#Dropdown10').html(article_html);

        });
    }
    dropdownList10();


    var dropdownList11 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#DropdownTemplate11").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#Dropdown11').html(article_html);

        });
    }
    dropdownList11();


    var dropdownList12 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#DropdownTemplate12").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#Dropdown12').html(article_html);

        });
    }
    dropdownList12();


    var dropdownList13 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#DropdownTemplate13").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#Dropdown13').html(article_html);

        });
    }
    dropdownList13();


    var dropdownList14 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#DropdownTemplate14").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#Dropdown14').html(article_html);

        });
    }
    dropdownList14();


    var dropdownList15 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#DropdownTemplate15").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#Dropdown15').html(article_html);

        });
    }
    dropdownList15();


    var dropdownList16 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#DropdownTemplate16").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#Dropdown16').html(article_html);

        });
    }
    dropdownList16();

    //  ================================================================== modal drop down templates


    var modaldropdownList = function() {
        $.getJSON("scripts/la-plans.json", function(data) {

            var templateSource = $("#ModalDropdownTemplate").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#ModalDropdown').html(article_html);
            ModalhideMasterPlanFalse();

        });
    }
    modaldropdownList(); // master plans


    var modaldropdownList2 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#ModalDropdownTemplate2").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#ModalDropdown2').html(article_html);

            hideNA();

        });
    }
    modaldropdownList2(); // oopm


    var modaldropdownList3 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#ModalDropdownTemplate3").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#ModalDropdown3').html(article_html);

        });
    }
    modaldropdownList3(); // silver HSA 3600 plan

    var modaldropdownList4 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#ModalDropdownTemplate4").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#ModalDropdown4').html(article_html);

        });
    }
    modaldropdownList4();



    var modaldropdownList5 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#ModalDropdownTemplate5").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#ModalDropdown5').html(article_html);

        });
    }
    modaldropdownList5();

    var modaldropdownList6 = function() {
        $.getJSON("scripts/la-plans.json", function(data) {

            var templateSource = $("#ModalDropdownTemplate6").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#ModalDropdown6').html(article_html);

        });
    }
    modaldropdownList6();

    var modaldropdownList7 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#ModalDropdownTemplate7").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#ModalDropdown7').html(article_html);

        });
    }
    modaldropdownList7();

    var modaldropdownList8 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#ModalDropdownTemplate8").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#ModalDropdown8').html(article_html);

        });
    }
    modaldropdownList8();


    var modaldropdownList9 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#ModalDropdownTemplate9").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#ModalDropdown9').html(article_html);

        });
    }
    modaldropdownList9();


    var modaldropdownList10 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#ModalDropdownTemplate10").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#ModalDropdown10').html(article_html);

        });
    }
    modaldropdownList10();


    var modaldropdownList11 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#ModalDropdownTemplate11").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#ModalDropdown11').html(article_html);

        });
    }
    modaldropdownList11();


    var modaldropdownList12 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#ModalDropdownTemplate12").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#ModalDropdown12').html(article_html);

        });
    }
    modaldropdownList12();


    var modaldropdownList13 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#ModalDropdownTemplate13").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#ModalDropdown13').html(article_html);

        });
    }
    modaldropdownList13();


    var modaldropdownList14 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#ModalDropdownTemplate14").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#ModalDropdown14').html(article_html);

        });
    }
    modaldropdownList14();


    var modaldropdownList15 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#ModalDropdownTemplate15").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#ModalDropdown15').html(article_html);

        });
    }
    modaldropdownList15();


    var modaldropdownList16 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#ModalDropdownTemplate16").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#ModalDropdown16').html(article_html);

        });
    }
    modaldropdownList16();
    //  =========================================== sub page gemodal drop down templates


    var subPage_modaldropdownList = function() {
        $.getJSON("scripts/la-plans.json", function(data) {

            var templateSource = $("#subPage_ModalDropdownTemplate").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#subPage_ModalDropdown').html(article_html);
            subPage_ModalhideMasterPlanFalse();

        });
    }
    subPage_modaldropdownList(); // master plans


    var subPage_modaldropdownList2 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#subPage_ModalDropdownTemplate2").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#subPage_ModalDropdown2').html(article_html);

            hideNA();

        });
    }
    subPage_modaldropdownList2(); // oopm


    var subPage_modaldropdownList3 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#subPage_ModalDropdownTemplate3").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#subPage_ModalDropdown3').html(article_html);

        });
    }
    subPage_modaldropdownList3(); // silver HSA 3600 plan

    var subPage_modaldropdownList4 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#subPage_ModalDropdownTemplate4").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#subPage_ModalDropdown4').html(article_html);

        });
    }
    subPage_modaldropdownList4();



    var subPage_modaldropdownList5 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#subPage_ModalDropdownTemplate5").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#subPage_ModalDropdown5').html(article_html);

        });
    }
    subPage_modaldropdownList5();

    var subPage_modaldropdownList6 = function() {
        $.getJSON("scripts/la-plans.json", function(data) {

            var templateSource = $("#subPage_ModalDropdownTemplate6").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#subPage_ModalDropdown6').html(article_html);

        });
    }
    subPage_modaldropdownList6();

    var subPage_modaldropdownList7 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#subPage_ModalDropdownTemplate7").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#subPage_ModalDropdown7').html(article_html);

        });
    }
    subPage_modaldropdownList7();

    var subPage_modaldropdownList8 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#subPage_ModalDropdownTemplate8").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#subPage_ModalDropdown8').html(article_html);

        });
    }
    subPage_modaldropdownList8();


    var subPage_modaldropdownList9 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#subPage_ModalDropdownTemplate9").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#subPage_ModalDropdown9').html(article_html);

        });
    }
    subPage_modaldropdownList9();


    var subPage_modaldropdownList10 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#subPage_ModalDropdownTemplate10").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#subPage_ModalDropdown10').html(article_html);

        });
    }
    subPage_modaldropdownList10();


    var subPage_modaldropdownList11 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#subPage_ModalDropdownTemplate11").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#subPage_ModalDropdown11').html(article_html);

        });
    }
    subPage_modaldropdownList11();


    var subPage_modaldropdownList12 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#subPage_ModalDropdownTemplate12").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#subPage_ModalDropdown12').html(article_html);

        });
    }
    subPage_modaldropdownList12();


    var subPage_modaldropdownList13 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#subPage_ModalDropdownTemplate13").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#subPage_ModalDropdown13').html(article_html);

        });
    }
    subPage_modaldropdownList13();


    var subPage_modaldropdownList14 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#subPage_ModalDropdownTemplate14").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#subPage_ModalDropdown14').html(article_html);

        });
    }
    subPage_modaldropdownList14();


    var subPage_modaldropdownList15 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#subPage_ModalDropdownTemplate15").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#subPage_ModalDropdown15').html(article_html);

        });
    }
    subPage_modaldropdownList15();


    var subPage_modaldropdownList16 = function() {
        $.getJSON("scripts/la-values.json", function(data) {

            var templateSource = $("#subPage_ModalDropdownTemplate16").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#subPage_ModalDropdown16').html(article_html);

        });
    }
    subPage_modaldropdownList16();
    // =========================================================================================
    var PlanDetailTitle = function() {
        $.getJSON("scripts/la-plans.json", function(data) {

            var templateSource = $("#PlanTitleTemplate").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#PlanTitle').html(article_html);

        });
    }
    PlanDetailTitle();

    Handlebars.registerHelper('if_eq', function(a, b, opts) {
        if (a == b) // Or === depending on your needs
            return opts.fn(this);
        else
            return opts.inverse(this);
    });

    var plantypelistData = function() {
        $.getJSON("scripts/la-plans.json", function(data) {

            var templateSource = $("#plantypelist").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#MetalicLevelPlans').html(article_html);

        });
    }
    plantypelistData();


    var plantypelistFooter = function() {
        $.getJSON("scripts/la-plans.json", function(data) {

            var templateSource = $("#plantypelistFooter").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);

            $('#FooterPlanSBCList').html(article_html);

        });
    }


    plantypelistFooter();



    //home page pland filtering 

    $(".FilterSection").on('click', '#homePlanFamInd', function() {
        $("#homePlanInd").prop('checked', false);

        //$("#homePlanFamInd").prop('checked', false);
        $("#Plans > li > div.OOPocket-Ind").fadeIn("easeOut");
        $("#Plans > li > div.Deduct-Fam").fadeIn("easeOut")
        $("#Plans > li > div.Deduct-Fam").fadeIn("easeOut");
        $("#KeyBar > div.planKey > div > div.typerow > div.fam").fadeIn("easeOut");
        $("#KeyBar > div.planKey > div > div.typerow > div.ind").animate({
            height: "70px"
        });
        $("#Plans > li > div.Deduct-Ind").animate({
            height: "70px"
        });

        $("#Plans > li > div.OOPocket-Fam").animate({
            height: "70px"
        });

        $(".plan-head").animate({
            height: "143px"
        });


    });

    $(".FilterSection").on('click', '#homePlanInd', function() {
        $("#homePlanFamInd").prop('checked', false);
        $("#Plans > li > div.OOPocket-Ind").fadeOut("easeOut");
        $("#Plans > li > div.Deduct-Fam").fadeOut("easeOut")
        $("#Plans > li > div.Deduct-Fam").fadeOut("easeOut");
        $("#KeyBar > div.planKey > div > div.typerow > div.fam").fadeOut("easeOut");
        $("#KeyBar > div.planKey > div > div.typerow > div.ind").animate({
            height: "100px"
        });
        $("#Plans > li > div.Deduct-Ind").animate({
            height: "100px"
        });

        $("#Plans > li > div.OOPocket-Fam").animate({
            height: "100px"
        });

        $("#deduct-title").animate({
            height: "98px"
        });

        $(".plan-head").animate({
            height: "100px"
        });

        $("#plans").animte({
            height: "424px"
        });

        // $("#KeyBar > div.planKey > div:nth-child(3)").animate({
        //     height:"101px"
        // });



    });






    // ==================================== Sub Page Filtering ===================

    $("#PlanTitle").on('click', '#IndRadio', function() {
        $("#FamRadio").prop('checked', false);
        $(".fam-cell-icon").fadeOut('slow');
        $(".fam-cell-content").fadeOut('slow');
        $(".ind-cell-content").delay(700).queue(function(nxt) {

            $(this).removeClass('col-md-4').addClass('col-md-9');
            nxt();
        });
    });

    $("#PlanTitle").on('click', '#FamRadio', function() {
        $("#IndRadio").prop('checked', false);
        $(".ind-cell-content").removeClass('col-md-9').addClass('col-md-4');
        $(".fam-cell-icon").delay(1500).fadeIn('slow');
        $(".fam-cell-content").delay(1500).fadeIn('slow');
    });



    var currentSet = 1;
    var nxtstate;

    console.log("current level, dynamically set:" + currentSet);

    var increaseState = function() {
        currentSet++;
        console.log("increase set to" + currentSet);
    }


    var prescincreaseState = function() {
        prescCurrentLevel++;
        console.log("increase set to" + currentSet);
    }

    var decreaseState = function() {
        currentSet--;
        console.log("decreased set to:" + currentSet);
    }

    var prescdecreaseState = function() {
        prescCurrentLevel--;
        console.log("decreased set to:" + currentSet);
    }

    var setNewState = function(state) {
        var s = state;
        currentSet = state;
        var ns = currentSet;
        console.log("new state set to " + ns);
    }

    var prescsetNewState = function(state) {
        var s = state;
        prescCurrentLevel = state;
        var ns = currentSet;
        console.log("new state set to " + ns);
    }

    var setstatetolast = function() {
        currentSet = 8;
        console.log("set state to last is " + currentSet + "should be 8");
        // showPrevThree(currentSet);
    }

    var prescsetstatetolast = function() {
        prescCurrentLevel = 8;
        console.log("set state to last is " + currentSet + "should be 8");
        // showPrevThree(currentSet);
    }

    var setstatetoFirst = function(state) {
        var cl = state;
        if (cl == 8) {
            condole.log("this is 8:" + cl);
        } else {}
    }



    var hideAndShowPlans = function() {
        var po = $(".planOption");
        po.hide();
        $(".planOption").show("easeIn"); //start off by showing first three
    }

    var hideAllPlans = function() {
        $(".planOption").hide("easeOut");
    }

    var preschideAllPlans = function() {
        $("#PrescriptionPlans > li").hide();
    }

    var showAllPlans = function() {
        $(".planOption").show("easeIn");
    }

    var precshowAllPlans = function() {
        $("#PrescriptionPlans > li").show("easeIn");
    }


    // ============================= show next and previous 3 ================================



    var setCurretArray = function(array) {
        currentArray = array;
    };


    var getCurrentArray = function() {
        return currentArray;
    };



    // plans table event handlers    

    var showNextThree = function(currentSet) {

        var curSet = currentSet;
        var easingFunction = "slow";
        var curSet = currentSet;
        console.log("the current set is" + curSet);


        if (curSet >= 8) {
            console.log("greater then 8");
            curSet = 0;
            console.log("reset, expect 0, set is:" + curSet);
            hideAllPlans()
            var startSlice = (curSet * 3);
            var endSlice = (startSlice + 3);
            console.log((startSlice) + " and " + (endSlice));
            $(".planOption:nth-child(" + (startSlice + 1) + ")").css("margin-left", "185px");
            $(".planOption").slice(startSlice, endSlice).show(easingFunction);
            curSet++;
            setNewState(curSet);

        } else if (curSet == 1) {
            console.log("current set is 1" + "really its at" + curSet)

            var startSlice = (curSet * 3);
            var endSlice = (startSlice + 3);
            console.log((startSlice) + " and " + (endSlice));
            $(".planOption:nth-child(" + (startSlice + 1) + ")").css("margin-left", "185px");
            $("#Plans > li:nth-child(4)").css("margin-left", "185px");
            hideAllPlans();
            $(".planOption").slice(startSlice, endSlice).show(easingFunction);
            curSet++;
            setNewState(curSet);
        } else {
            var curSet = currentSet;
            var startSlice = (curSet * 3);
            var endSlice = (startSlice + 3);
            console.log((startSlice) + " and " + (endSlice));
            $(".planOption:nth-child(" + (startSlice + 1) + ")").css("margin-left", "185px");
            hideAllPlans();
            $(".planOption").slice(startSlice, endSlice).show(easingFunction);
            curSet++;
            setNewState(curSet);

        }
    }



    var showPrevThree = function(currentSet) {
        var easingFunction = "slow";
        var newstate = currentSet;
        if (newstate == 1) {
            newstate = 8;
            setNewState(newstate);
            var endSlice = (newstate * 3);
            var startSlice = (endSlice - 3);
            console.log((startSlice) + " and " + (endSlice));
            $(".planOption:nth-child(" + (startSlice + 1) + ")").css("margin-left", "185px");
            hideAllPlans()
            $(".planOption").slice(startSlice, endSlice).show(easingFunction);


        } else if (newstate > 1) {
            console.log(newstate);
            newstate--;
            console.log(newstate);
            setNewState(newstate);
            var endSlice = (newstate * 3);
            var startSlice = (endSlice - 3);
            console.log((startSlice) + " and " + (endSlice));
            $(".planOption:nth-child(" + (startSlice + 1) + ")").css("margin-left", "185px");
            hideAllPlans()
            $(".planOption").slice(startSlice, endSlice).show(easingFunction);

        }

    }


    // presc table event handlers / carousel btn

    var prescCurrentLevel = 1;
    console.log("presc current level, dynamically set:" + prescCurrentLevel);

    var prescShowNextThree = function(currentSet) {


        var easingFunction = "slow";

        var curSet = currentSet;
        console.log("the current set is" + curSet);
        if (curSet >= 8) {
            console.log("greater then 8");
            curSet = 0;
            console.log("reset, expect 0, set is:" + curSet);
            var startSlice = (curSet * 3);
            var endSlice = (startSlice + 3);
            console.log((startSlice) + " and " + (endSlice));
            $(".prescPlanoption:nth-child(" + (startSlice + 1) + ")").css("margin-left", "159px");
            preschideAllPlans();
            $(".prescPlanoption").slice(startSlice, endSlice).show(easingFunction);

            curSet++;
            prescsetNewState(curSet);

        } else {
            var curSet = currentSet;

            var startSlice = (curSet * 3);
            var endSlice = (startSlice + 3);
            console.log((startSlice) + " and " + (endSlice));
            $(".prescPlanoption:nth-child(" + (startSlice + 1) + ")").css("margin-left", "159px");
            preschideAllPlans();
            $(".prescPlanoption").slice(startSlice, endSlice).show(easingFunction);

            prescincreaseState();

        }
    }


    var prescShowPrevThree = function(currentSet) {
        var easingFunction = "slow";
        var newstate = currentSet;
        if (newstate == 1) {
            newstate = 8;
            prescsetNewState(newstate);
            var endSlice = (newstate * 3);
            var startSlice = (endSlice - 3);
            console.log((startSlice) + " and " + (endSlice));
            $(".prescPlanoption:nth-child(" + (startSlice + 1) + ")").css("margin-left", "159px");
            preschideAllPlans()
            $(".prescPlanoption").slice(startSlice, endSlice).show(easingFunction);


        } else if (newstate > 1) {
            console.log(newstate);
            newstate--;
            console.log(newstate);
            prescsetNewState(newstate);
            var endSlice = (newstate * 3);
            var startSlice = (endSlice - 3);
            console.log((startSlice) + " and " + (endSlice));
            $(".prescPlanoption:nth-child(" + (startSlice + 1) + ")").css("margin-left", "159px");
            preschideAllPlans()
            $(".prescPlanoption").slice(startSlice, endSlice).show(easingFunction);

        }

    }





    // ======================================== carousel btns ========================================

    // ============================== PLANS

    var nextBtn = $("#arrow-right");
    var prevBtn = $("#arrow-left");

    nextBtn.on("click", function() {

        showNextThree(currentSet);
    });

    prevBtn.on("click", function() {
        showPrevThree(currentSet);
    });



    // =================================== PRESC



    var precnextBtn = $("#presc-arrow-right");
    var precprevBtn = $("#presc-arrow-left");

    precnextBtn.on("click", function() {
        console.log("prec  next hit ");
        prescShowNextThree(prescCurrentLevel);
    });

    precprevBtn.on("click", function() {
        console.log("prec prev hit ");
        prescShowPrevThree(prescCurrentLevel);
    });




    // these functions are used  to toggle off other check&radio btns when on is clicked
    $("input[type='checkbox']").on("change", function() {
        if (this.checked) {
            $(this).closest('.FilterSection')
                .find('input[type=checkbox]').not(this)
                .prop('checked', false);
        } else {}
    });

    $("input[type='radio']").on("change", function() {
        if (this.checked) {
            $(this).closest('.FilterSection')
                .find('input[type=radio]').not(this)
                .prop('checked', false);
        } else {}
    });








    // ========================== smooth scrolling function ====================

    var $root = $('html, body');
    $('.mainnav > div > ul > li > a').click(function() {
        var href = $.attr(this, 'href');
        $root.animate({
            scrollTop: $(href).offset().top
        }, "easeOut", function() {
            window.location.hash = href;
        });
        return false;
    });

    var $root = $('html, body');
    $('#section1 > div > div > div > a').click(function() {
        var href = $.attr(this, 'href');
        $root.animate({
            scrollTop: $(href).offset().top
        }, "easeOut", function() {
            window.location.hash = href;
        });
        return false;
    });

    $('#FatFooter > div.iconrow.hidden-xs > div > div > div > div > a').click(function() {
        var href = $.attr(this, 'href');
        $root.animate({
            scrollTop: $(href).offset().top
        }, "easeOut", function() {
            window.location.hash = href;
        });
        return false;
    });



    // ===================================== autocomplete data array ============================ 
    $(function() {

        var availableTags = [{
            value: "NYC",
            url: 'http://www.nyc.com'
        }, {
            value: "LA",
            url: 'http://www.la.com'
        }, {
            value: "Philly",
            url: 'http://www.philly.com'
        }, {
            value: "Chitown",
            url: 'http://www.chitown.com'
        }, {
            value: "DC",
            url: 'http://www.washingtondc.com'
        }, {
            value: "SF",
            url: 'http://www.sanfran.com'
        }, {
            value: "Peru",
            url: 'http://www.peru.com'
        }];
        // invoke jquery ui for autocomplete 

        $("#tags").autocomplete({
            source: availableTags,
            autoFocus: true,
            minLength: 0,
            select: function(event, ui) {
                go(ui.item.url);
            },

            focus: function(event, ui) {
                currentUrl = ui.item.url;
            }
        });
    });

    function go(url) {
        window.open(url);
    }

    function btnGoClick() {
        if (currentUrl !== "") go(currentUrl);
    }



    // caret button that trigger/enables the rendering of the autocomplete button
    var button = $("#dropDownToggle");
    button.click(function(event) {
        var input = $("#tags");
        input.focus();
        input.autocomplete("search", "");
        console.log("caret clicked");
    });


    // =================================== video reveal function ==============================

    // responsive video

    var video = $(".video-player");
    var windowObj = $(window);

    function onResizeWindow() {
        resizeVideo(video[0]);
    }

    function onLoadMetaData(e) {
        resizeVideo(e.target);
    }

    function resizeVideo(videoObject) {
        var percentWidth = videoObject.clientWidth * 100 / videoObject.videoWidth;
        var videoHeight = videoObject.videoHeight * percentWidth / 100;
        video.height(videoHeight);
    }

    video.on("loadedmetadata", onLoadMetaData);
    windowObj.resize(onResizeWindow);


    // Var  playStop = function(){

    // }

    function videoRevel(IdOfVideoDiv, IdofBtn, clickedclass, textEl) {
        var clickedclass = clickedclass;
        var video = IdOfVideoDiv;
        var btn = IdofBtn;
        var classToggle = clickedclass;
        var txtEL = textEl;
        btn.click(function() {

            video.slideToggle("easeOut");
            btn.toggleClass(classToggle);
           


        });

        // if(textNode.text() == "See More"){
        //     var sl = "See Less";
        //     textNode.text(sl);
        // }

        // else if(textNode.text() == "See Less"){
        //     var sl = "See More";
        //     textNode.text(sl);
        // }

    }

    // ============================== needs to be refactored DRY ===============================


    //section 1 video and btn
    var sec1video = $('#video-player-sec1');
    var sec2btn = $('#sec1btn-vid');
    sec1video.fadeOut("fast");
    sec1video.on("Click", videoRevel(sec1video, sec2btn, "fa-youtube-play"));

    //section 3 arizona details see more 
    var sec3btn = $("#arz-seemore-btn");
    var sec3div = $("#arz-plan-details");
    sec3div.fadeOut("fast");
    sec3btn.on("Click", videoRevel(sec3div, sec3btn, "fa-minus-square"));

    //section 3 video reveal

    var sec3VidBtn = $("#sec3-vid-btn");
    var sec3VidDiv = $("#video-player-sec3");
    sec3VidDiv.fadeOut("fast");
    sec3VidBtn.on("Click", videoRevel(sec3VidDiv, sec3VidBtn, "fa-youtube-play"));

    //sec 4 more details reveal
    var sec4BTn = $("#sec4seeMoreBTn");
    var sec4DivCont = $(".sec4-hidden-cont");
    var btnText = $(".seemoreText");
    sec4DivCont.fadeOut("fast");
    sec4BTn.on("Click", videoRevel(sec4DivCont, sec4BTn, "fa-minus-square", btnText));


    // section 4 video reveal
    var sec4VidBtn = $('#sec4-vid-btn');
    var sec4VidCont = $('#sec4-video');
    sec4VidCont.fadeOut("fast");
    sec4VidBtn.on("Click", videoRevel(sec4VidCont, sec4VidBtn, "fa-youtube-play"));

    // section 5 cont reveal
    var sec5contBtn = $('#sec5seeMoreBtn');
    var sec5DivCont = $('#sec5HiddenContent');
    sec5DivCont.fadeOut("fast");
    sec5contBtn.on("Click", videoRevel(sec5DivCont, sec5contBtn, "fa-minus-square"));

    //sec 5 video reveal
    var sec5vidBtn = $('#sec5-vid-btn');
    var sec5DivVid = $('#video-player-sec5');
    sec5DivVid.fadeOut("fast");
    sec5vidBtn.on("Click", videoRevel(sec5DivVid, sec5vidBtn, "fa-youtube-play"));


    //section 6 vid & btn
    var sec6btn = $('#sec6-vid-btn');
    var sec6Div = $('#sec6-video');
    sec6Div.fadeOut("fast");
    sec6btn.on("Click", videoRevel(sec6Div, sec6btn, "fa-youtube-play"));

    //section 6 vid & btn
    var sec7btn = $('#claims-vid-btn');
    var sec7Div = $('#claims-video');
    sec7Div.fadeOut("fast");
    sec7btn.on("Click", videoRevel(sec7Div, sec7btn, "fa-youtube-play"));

    //search reveal 
    var searchBtn = $("#searchBtn"); 
    var searchDiv = $('#search');
    searchDiv.fadeOut("fast");
    searchBtn.on("click", function(){
        searchDiv.slideToggle();
    });
    /* ================================    more details button   ===========================*/



    // used for icon switchin from postaive icon button to negative.

    function switchIcon(el, sClass) {
        var switchClass = sClass;
        var btn = el;
        btn.on("click", function() {
            $(this).toggleClass(switchClass)
        });

    };


    var moreDetailsBtn = $('.moreDetailsBtn');
    moreDetailsBtn.on("Click", switchIcon(moreDetailsBtn, "fa-minus-square"));



    // =================================== footer reveal function =============================
    $("#FatFooter").hide();

    $(".dropsprite").click(function() {

        $("#FatFooter").slideToggle();
        $('html, body').animate({
            scrollTop: $(document).height()
        }, 'slow');
        $(this).toggleClass("dropsprite-down");
        $(this).toggleClass("dropsprite-up");
    });


    $('body').scrollspy({
        target: '.navbar-collapse'
    });







}); //end of doc.ready function
